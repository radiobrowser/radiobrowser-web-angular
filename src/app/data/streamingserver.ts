export interface DataStreamingServer {
  uuid: string;
  url: string;
  statusurl: string;
  error: string;
  server_software: string;
  admin: string;
  location: string;
}
