import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', loadComponent: () => import('./components/main/main.component') },
  { path: 'faq', loadComponent: () => import('./components/faq/faq.component') },
  { path: 'users', loadComponent: () => import('./components/users/users.component') },
  { path: 'owners', loadComponent: () => import('./components/owners/owners.component') },
  { path: 'tags', loadComponent: () => import('./components/taglist/taglist.component') },
  { path: 'countries', loadComponent: () => import('./components/countrylist/countrylist.component') },
  { path: 'languages', loadComponent: () => import('./components/languagelist/languagelist.component') },
  { path: 'codecs', loadComponent: () => import('./components/codeclist/codeclist.component') },
  { path: 'map', loadComponent: () => import('./components/geomap/geomap.component') },
  { path: 'search', loadComponent: () => import('./components/search/search.component') },
  { path: 'add', loadComponent: () => import('./components/newstation/newstation.component') },
  { path: 'history/:id', loadComponent: () => import('./components/details/details.component') },
];
