import { Component } from '@angular/core';

@Component({
  selector: 'app-owners',
  standalone: true,
  templateUrl: './owners.component.html',
  styleUrls: ['./owners.component.css'],
})
export default class OwnersComponent {}
