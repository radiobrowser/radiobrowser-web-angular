import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { DataServerStats } from 'src/app/data/server-stats';
import { RadiobrowserService } from 'src/app/services/radiobrowser.service';

@Component({
  selector: 'app-main',
  standalone: true,
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  imports: [RouterLink, NgIf]
})
export default class MainComponent implements OnInit {

  stats: DataServerStats = null;

  constructor(private rbservice: RadiobrowserService) { }

  getStations(): void {
    this.rbservice.getStats()
      .subscribe(stats => { this.stats = stats; });
  }

  ngOnInit() {
    this.getStations();
  }

}
